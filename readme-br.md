# GLPI atualização de regras para movimentação de chamados

O GLPI nativamente (até a última release 10.0.6) não permite que uma regra de negócios para chamados tenha uma Entidade na lista de ações.

![image](img1.png)


A 'regra de negócio para chamado' não permite a seleção de uma entidade na lista de ações.

![image](img2.png)


A alteração do módulo RuleTicket permite a movimentação de tickets entre diferentes entidades.

A atualização é realizada no arquivo:
```shell
.../src/RuleTicket.php
```


A função getActions é modificada incluindo a seleção de entidades nas actions.

```php

//736
public function getActions() {

//acrescentar
$actions['entities_id']['name'] = Entity::getTypeName(1);
$actions['entities_id']['type'] = 'dropdown';
$actions['entities_id']['table'] = Entity::getTable();
$actions['itilfollowup_template']['force_actions'] = ['append'];
$actions['itilfollowup_template']['permitseveral'] = ['append'];

return $actions;

}
```

Como resultado, as Entidades aparecem na lista de ações.

![image](img3.png)

Para ter o efeito desejado é necessário ter ajustes adequados nas permissões de grupos e usuários.