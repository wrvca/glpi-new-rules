# Updating rules for moving calls

GLPI natively (until the latest release 10.0.6) does not allow a business rule for tickets to have an Entity in the action list.
This update allows the transfer of a ticket to a specific entity.

![image](img1-1.png)


The 'business rule for ticket' does not allow selecting an entity from the action list.


![image](img2-2.png)


Changing the RuleTicket module allows moving tickets between different entities.

The update is performed in the file:
```shell
.../src/RuleTicket.php
```


The getActions function is modified by including the selection of entities in the actions.

```php

//736
public function getActions() {

//add
$actions['entities_id']['name'] = Entity::getTypeName(1);
$actions['entities_id']['type'] = 'dropdown';
$actions['entities_id']['table'] = Entity::getTable();
$actions['itilfollowup_template']['force_actions'] = ['append'];
$actions['itilfollowup_template']['permitseveral'] = ['append'];

return $actions;

}
```

As a result, Entities appear in the action list.

![image](img3-3.png)


To have the desired effect it is necessary to have adequate adjustments in the permissions of groups and users.

